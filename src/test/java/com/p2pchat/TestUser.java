package com.p2pchat;

import model.User;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class TestUser {
    private User user;

    @Before
    public void setup() {
        user = new User("test");
    }

    @Test
    public void testUserEquals() {
        User user1 = new User("test");
        User user2 = new User("test");
        User user3 = new User("testing");

        assertEquals(user, user1);
        assertEquals(user, user2);
        assertEquals(user1, user2);
        assertNotEquals(user, user3);
        assertNotEquals(user1, user3);
        assertNotEquals(user2, user3);
    }
}
