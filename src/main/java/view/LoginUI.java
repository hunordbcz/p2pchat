package view;

import controller.UserController;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class LoginUI extends AbstractJFrame {
    private JPanel loginPanel;
    private JButton enterButton;
    private final UserController userController;
    private JTextField nameField;

    public LoginUI() {
        userController = new UserController();

        enterButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                userController.login(nameField.getText());
            }
        });
    }

    @Override
    public JPanel getPanel() {
        return loginPanel;
    }
}
