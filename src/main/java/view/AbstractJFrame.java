package view;

import controller.RouteController;

import javax.swing.*;

public abstract class AbstractJFrame extends JFrame {
    RouteController router = RouteController.getInstance();

    public abstract JPanel getPanel();

    public void setArgs(Object[] args) {
        throw new IllegalStateException("Not implemented");
    }
}
