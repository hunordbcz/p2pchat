package view;

import controller.ChatController;
import model.Message;
import model.State;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ChatUI extends AbstractJFrame {
    private JButton sendButton;
    private JPanel chatPanel;
    private JScrollPane messageContainer;
    private JTextArea textArea1;
    private JTextArea chatBox;

    public ChatUI(ChatController chatController) {
        sendButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Message message = new Message(textArea1.getText(), State.getUser());
                chatController.addMessage(message);
                chatController.addMessageToUI(message);
                textArea1.setText("");
            }
        });
    }

    @Override
    public JPanel getPanel() {
        return chatPanel;
    }

    public void addMessage(Message message) {
        chatBox.append("<" + message.getFrom().getName() + ">:  " + message.getMessage() + "\n");
    }
}
