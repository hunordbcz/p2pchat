package view;

import controller.ConnectionController;
import controller.UserController;
import model.Chat;
import model.State;
import model.User;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class UsersUI extends AbstractJFrame {
    private JPanel usersPanel;
    private JList<User> usersList;
    private JButton sayHelloOpenChatButton;
    private final ConnectionController connectionController;

    public UsersUI() {
        connectionController = new ConnectionController();

        DefaultListModel<User> listModel = new DefaultListModel<>();
        UserController.getUsers().forEach((k, v) -> {
            if (!v.equals(State.getUser())) {
                listModel.addElement(v);
            }
        });
        listModel.addElement(new User("cristi"));
        listModel.addElement(new User("alex"));
        listModel.addElement(new User("ruxi"));
        listModel.addElement(new User("hunor"));
        usersList.setModel(listModel);

        ListSelectionModel usersSelection = usersList.getSelectionModel();

        sayHelloOpenChatButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Chat chat = new Chat(State.getUser(), usersList.getSelectedValue());
                connectionController.addChat(chat);
            }
        });

        usersSelection.addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                sayHelloOpenChatButton.setEnabled(false);
                if (e.getValueIsAdjusting()) {
                    return;
                }

                int selectedIndex = usersList.getSelectedIndex();
                if (selectedIndex < 0) {
                    return;
                }

                sayHelloOpenChatButton.setEnabled(true);
            }
        });
    }

    @Override
    public JPanel getPanel() {
        return usersPanel;
    }
}
