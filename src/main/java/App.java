import controller.RouteController;
import view.LoginUI;

public class App {
    public static void main(String[] args) {
        RouteController.getInstance().redirect(new LoginUI());
    }
}
