package model;

public class Chat {
    private final User me;
    private final User other;


    public Chat(User me, User other) {
        this.me = me;
        this.other = other;
    }

    public User getMe() {
        return me;
    }

    public User getOther() {
        return other;
    }
}
