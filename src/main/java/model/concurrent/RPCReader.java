package model.concurrent;

import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.DeliverCallback;
import controller.ChatController;
import model.*;
import org.json.JSONObject;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;

public class RPCReader implements Runnable {

    private final String queueName;
    private final ConcurrentHashMap<User, ChatController> chats;
    private final ExecutorService executorService;

    public RPCReader(String queueName, ConcurrentHashMap<User, ChatController> chats, ExecutorService executorService) {
        this.queueName = queueName;
        this.chats = chats;
        this.executorService = executorService;
    }

    @Override
    public void run() {
        MyConnection conn = new MyConnection();
        Channel channel = conn.getChannel();

        try {
            channel.queueDeclare(queueName, false, false, false, null);
            channel.queuePurge(queueName);

            channel.basicQos(1);

            System.out.println(" [x] Awaiting RPC requests " + queueName);

            Object monitor = new Object();
            DeliverCallback deliverCallback = (consumerTag, delivery) -> {
                AMQP.BasicProperties replyProps = new AMQP.BasicProperties
                        .Builder()
                        .correlationId(delivery.getProperties().getCorrelationId())
                        .build();

                String response = "";
                String json = new String(delivery.getBody(), StandardCharsets.UTF_8);

                System.out.println("Received" + json);

                JSONObject object = new JSONObject(json);

                User from = new User(object.getString("from"));
                Message message = new Message(object.getString("message"), from);

                Chat chat = new Chat(State.getUser(), from);
                ChatController chatController = chats.get(from);
                if (chatController != null) {
                    chatController.addMessageToUI(message);
                } else {
                    chatController = new ChatController(chat);
                    executorService.submit(chatController);
                    chats.put(from, chatController);
                }

                try {
                    response += "!ack";
                } catch (RuntimeException e) {
                    System.out.println(" [.] " + e.toString());
                } finally {
                    try {
                        channel.basicPublish("", delivery.getProperties().getReplyTo(), replyProps, response.getBytes(StandardCharsets.UTF_8));
                        channel.basicAck(delivery.getEnvelope().getDeliveryTag(), false);
                        // RabbitMq consumer worker thread notifies the RPC server owner thread
                        synchronized (monitor) {
                            monitor.notify();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            };

            channel.basicConsume(queueName, false, deliverCallback, (consumerTag -> {
            }));
            // Wait and be prepared to consume the message from RPC client.

            //todo stop
            while (true) {
                synchronized (monitor) {
                    try {
                        monitor.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
