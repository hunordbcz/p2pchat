package model.concurrent;

import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import controller.ChatController;
import model.MyConnection;
import model.User;

import java.nio.charset.StandardCharsets;
import java.util.UUID;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentHashMap;

public class RPCSender implements Callable<String> {

    private final String requestQueueName;
    private final String json;

    private final ConcurrentHashMap<User, ChatController> chats;

    public RPCSender(String queueName, String json, ConcurrentHashMap<User, ChatController> chats) {
        this.requestQueueName = queueName;
        this.json = json;
        this.chats = chats;
    }

    @Override
    public String call() throws Exception {
        MyConnection conn = new MyConnection();
        Channel channel = conn.getChannel();

        final String corrId = UUID.randomUUID().toString();

        String replyQueueName = channel.queueDeclare().getQueue();
        AMQP.BasicProperties props = new AMQP.BasicProperties
                .Builder()
                .correlationId(corrId)
                .replyTo(replyQueueName)
                .build();

        System.out.println("Sent " + json);
        channel.basicPublish("", requestQueueName, props, this.json.getBytes(StandardCharsets.UTF_8));

        if (chats != null && !chats.containsKey(new User(requestQueueName))) {
            final BlockingQueue<String> response = new ArrayBlockingQueue<>(1);

            String ctag = channel.basicConsume(replyQueueName, true, (consumerTag, delivery) -> {
                if (delivery.getProperties().getCorrelationId().equals(corrId)) {
                    response.offer(new String(delivery.getBody(), StandardCharsets.UTF_8));
                }
            }, consumerTag -> {
            });

            String result = response.take();
            channel.basicCancel(ctag);
            System.out.println(result);
            return result;
        }

        return "";
    }
}
