package model;

import view.AbstractJFrame;

public class State {
    private static User user;
    private static AbstractJFrame activeFrame;

    private State() {

    }

    public static User getUser() {
        return user;
    }

    public static void setUser(User user) {
        State.user = user;
    }

    public static AbstractJFrame getActiveFrame() {
        return activeFrame;
    }

    public static void setActiveFrame(AbstractJFrame activeFrame) {
        State.activeFrame = activeFrame;
    }
}
