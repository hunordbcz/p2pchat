package model;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import java.io.IOException;
import java.net.URISyntaxException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.concurrent.TimeoutException;

public class MyConnection {
    private Channel channel;

    public MyConnection() {

        final String uri = "amqp://iijxrbbq:rnaStNIloFgQAQXtvYBk7A612BQelBr8@chinook.rmq.cloudamqp.com/iijxrbbq";
        ConnectionFactory factory = new ConnectionFactory();
        try {
            factory.setUri(uri);
            factory.setRequestedHeartbeat(30);
            factory.setConnectionTimeout(30000);

            Connection connection = factory.newConnection();
            channel = connection.createChannel();
        } catch (URISyntaxException | NoSuchAlgorithmException | KeyManagementException | TimeoutException | IOException e) {
            e.printStackTrace();
        }

    }

    public Channel getChannel() {
        return channel;
    }
}