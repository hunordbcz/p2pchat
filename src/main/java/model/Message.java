package model;

public class Message {
    private final String message;
    private final User from;

    public Message(String message, User from) {
        this.message = message;
        this.from = from;
    }

    public String getMessage() {
        return message;
    }

    public User getFrom() {
        return from;
    }
}
