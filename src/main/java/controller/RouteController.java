package controller;

import model.State;
import view.AbstractJFrame;

import javax.swing.*;

public class RouteController {

    private RouteController() {

    }

    public static RouteController getInstance() {
        return Singleton.INSTANCE;
    }

    public void redirect(AbstractJFrame nextFrame, Object... args) {
        AbstractJFrame currentFrame = State.getActiveFrame();

        String title;
        if (State.getUser() != null) {
            title = State.getUser().getName();
        } else {
            title = nextFrame.getClass().getSimpleName();
        }

        nextFrame.setTitle(title);
        nextFrame.setContentPane(nextFrame.getPanel());
        nextFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        nextFrame.pack();

        if (currentFrame == null) {
            nextFrame.setLocationRelativeTo(null);
        } else {
            nextFrame.setLocation(currentFrame.getX(), currentFrame.getY());
            currentFrame.dispose();
        }

        if (args.length != 0) {
            nextFrame.setArgs(args);
        }

        nextFrame.setVisible(true);
        State.setActiveFrame(nextFrame);
    }

    public void popup(String message, int type) {
        if (State.getActiveFrame() == null) {
            return;
        }

        JOptionPane.showMessageDialog(State.getActiveFrame(), message, State.getActiveFrame().getTitle() + " Notification", type);
    }

    private static class Singleton {
        private static final RouteController INSTANCE = new RouteController();
    }
}
