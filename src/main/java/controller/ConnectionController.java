package controller;

import model.Chat;
import model.Message;
import model.State;
import model.User;
import model.concurrent.RPCReader;
import model.concurrent.RPCSender;
import org.json.JSONObject;

import javax.swing.*;
import java.util.concurrent.*;

public class ConnectionController {
    private final static int MAX_CHATS = 25;

    private static final ConcurrentHashMap<User, ChatController> chatControllers = new ConcurrentHashMap<>();
    private static final ExecutorService exec = Executors.newFixedThreadPool(MAX_CHATS);
    private final ExecutorService chatInitializer;

    public ConnectionController() {
        this.chatInitializer = Executors.newFixedThreadPool(4);

        chatInitializer.submit(new RPCReader(State.getUser().getName().toLowerCase(), chatControllers, exec));
    }

    public static void closeAll() {
        Message message = new Message("!byebye", State.getUser());
        chatControllers.forEach((k, v) -> {
//            v.addMessage(message);
            v.closeChat();
        });
        exec.shutdown();
    }

    public static void close(User user) {
        ChatController chat = chatControllers.get(user);
        chat.closeChat();
        chatControllers.remove(user);
    }

    public boolean addChat(Chat chat) {
        String name = chat.getOther().getName();
        JSONObject json = new JSONObject();
        json.put("from", chat.getMe().getName());
        json.put("message", "!hello " + name);

        try {
            Future<String> result = chatInitializer.submit(new RPCSender(name, json.toString(), chatControllers));

            if (!chatControllers.containsKey(chat.getOther())) {
                try {
                    String response = result.get(5, TimeUnit.SECONDS);

                    if (!response.equals("!ack")) {
                        RouteController.getInstance().popup("Not acknowledged", JOptionPane.ERROR_MESSAGE);
                        return false;
                    }

                    ChatController chatController = new ChatController(chat);
                    chatControllers.put(chat.getOther(), chatController);
                    exec.submit(chatController);
                } catch (TimeoutException e) {
                    RouteController.getInstance().popup("Time limit reached, " + chat.getOther().getName() + " didn't respond", JOptionPane.ERROR_MESSAGE);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return true;
    }

}
