package controller;

import model.Chat;
import model.Message;
import model.concurrent.RPCSender;
import org.json.JSONObject;
import view.ChatUI;

import javax.swing.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ChatController implements Runnable {
    private final ExecutorService exec = Executors.newFixedThreadPool(2);
    private final Chat chat;
    private final ChatUI window;

    public ChatController(Chat chat) {
        this.chat = chat;
        this.window = new ChatUI(this);
    }

    @Override
    public void run() {
        window.setTitle(chat.getOther().getName() + " - " + chat.getMe().getName() + " (Me)");
        window.setContentPane(window.getPanel());
        window.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        window.pack();
        window.setLocationRelativeTo(null);
        window.setVisible(true);
    }

    public void sendMessage(String queueName, String json) {
        exec.submit(new RPCSender(queueName, json, null));
    }

    public void addMessage(Message message) {
        JSONObject object = new JSONObject();
        object.put("message", message.getMessage());
        object.put("from", chat.getMe().getName());

        sendMessage(chat.getOther().getName(), object.toString());
    }

    public void addMessageToUI(Message message) {
        window.addMessage(message);
        if (message.getMessage().contains("!byebye")) {
            ConnectionController.closeAll();
        }
        else if (message.getMessage().contains("!bye")) {
            ConnectionController.close(chat.getOther());
        }
    }

    public void closeChat() {
        window.dispose();
        Thread.currentThread().interrupt();
    }
}
