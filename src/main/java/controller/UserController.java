package controller;

import model.State;
import model.User;
import view.UsersUI;

import javax.swing.*;
import java.util.concurrent.ConcurrentHashMap;

public class UserController {
    private static final ConcurrentHashMap<String, User> users = new ConcurrentHashMap<>();
    private static final RouteController router = RouteController.getInstance();

    public UserController() {

    }

    public static ConcurrentHashMap<String, User> getUsers() {
        return users;
    }

    public void login(String name) {
        if (!validate(name)) {
            router.popup("Invalid name", JOptionPane.ERROR_MESSAGE);
        }

        User user = new User(name);
        if (!attempt(user)) {
            router.popup("User with that name already exists", JOptionPane.ERROR_MESSAGE);
        }

        router.redirect(new UsersUI());
    }

    private boolean attempt(User user) {
        if (users.contains(user)) {
            return false;
        }

        addUser(user);
        State.setUser(user);
        return true;
    }

    private boolean validate(String name) {
        return true;
    }

    public void addUser(User user) {
        users.put(user.getName(), user);
    }
}
